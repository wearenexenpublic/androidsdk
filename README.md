# Nexen Beacon SDK for Android #

This document outlines the necessary steps in order to seamlessly integrate the Nexen Android SDK into your own
Android projects.

## Configuring your Android project ##

The SDK heavily relies on the [Android Beacon Library](http://altbeacon.github.io/android-beacon-library/) 
that is developed by RadiusNetworks. Feel free to head over to their website in order to learn more about the 
low-level architecture of region monitoring and beacon ranging.

### Compile-time dependencies ###

In order to be able to successfully compile your app against the SDK you will need to include at least these two
dependencies:

-   the AltBeacon Java library

    This library manages the low-level details of connecting with the Bluetooth LE subsystem and scanning for beacons.
     
-   The Nexen Android SDK

    The SDK wraps the AltBeacon library and makes an abstraction in order for users not to worry about the
    details of the AltBeacon library. In addition, the SDK will regularly connect to the Nexen server and
    download content such as which beacons to scan and what to show in case a beacon is detected. Your application
    will simply need to define what to do when the SDK tells you when a new beacon has been detected.
    
-   the Retrofit library
    
    Retrofit is type-safe HTTP client for Android and Java. Also you will need some extra dependencies required by retrofit:
    converter-gson, converter-jackson, jackson-datatype-joda, logging-interceptor

In case you are using Gradle to build your project you would add the following to your `<dependencies>` element 
(you might need to update the version numbers in case new updates have been released):

    dependencies {
		api 'com.google.android.gms:play-services-location:8.4.0'
		api 'com.android.support:support-v4:26.0.2'
		api 'com.android.support:support-compat:26.0.2'
		api 'org.altbeacon:android-beacon-library:2.15.1'
		api 'pl.charmas.android:android-reactive-location:0.9@aar'
		api 'io.reactivex:rxjava:1.1.5'
		api 'io.reactivex:rxandroid:0.25.0'
		api 'com.artemzin.rxjava:proguard-rules:1.1.5.0'
		api 'com.squareup.retrofit2:retrofit:2.1.0'
		api 'com.squareup.retrofit2:converter-gson:2.1.0'
		api 'com.squareup.retrofit2:converter-jackson:2.1.0'
		api 'com.fasterxml.jackson.datatype:jackson-datatype-joda:2.8.5'
		api 'com.squareup.okhttp3:logging-interceptor:3.4.1'
		api 'com.github.bumptech.glide:glide:3.7.0'
    }

### AndroidManifest.xml ###

The Android Beacon Library is compatible with 
[Android API version 18 (Jelly Bean MR2)](http://developer.android.com/about/versions/android-4.3.html) and above.

    <uses-sdk android:minSdkVersion="15"/>

In order to get started with Bluetooth LE and scanning beacons your Android application will need to enable the
corresponding features.

    <!-- these features should be required, no point in having the app if the device does not support Bluetooth -->
    <uses-feature android:name="android.hardware.bluetooth" android:required="true"/>
    <uses-feature android:name="android.hardware.bluetooth_le" android:required="true"/>
    
    <!-- your app will down load content from the Nexen server -->
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <!-- the SDK keeps track of downloaded content and needs to store it to disk -->
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <!-- you're probably downloading beacon locations in order to show them on a map -->
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    <uses-permission android:name="android.permission.BLUETOOTH"/>
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
    <!-- to enable the app to startup when a beacon is detected -->
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />

Within your `<application>` element there are some additional elements

    <receiver android:name="org.altbeacon.beacon.startup.StartupBroadcastReceiver" >
        <intent-filter>
            <action android:name="android.intent.action.BOOT_COMPLETED" />
            <action android:name="android.intent.action.ACTION_POWER_CONNECTED" />
            <action android:name="android.intent.action.ACTION_POWER_DISCONNECTED" />
        </intent-filter>
    </receiver>
    
    <receiver android:name="be.wearenexen.NexenProximityManager$NexenBroadcastReceiver" android:enabled="true"/>

    <receiver android:name=".BluetoothStateChangeBroadcastReceiver" >
            <intent-filter>
                <action android:name="android.bluetooth.adapter.action.STATE_CHANGED" />
            </intent-filter>
    </receiver>
    
    <service
        android:name="org.altbeacon.beacon.service.BeaconService"
        android:enabled="true"
        android:exported="false"
        android:isolatedProcess="false"
        android:label="beacon" />
    <service
        android:name="org.altbeacon.beacon.BeaconIntentProcessor"
        android:enabled="true"
        android:exported="false" />

***You won't need the `StartupBroadcastReceiver` in case you don't want beacons to trigger a launch of your application,
even after your device has booted. If you do include it then you should make sure your application's launch intent
is tied to a single activity instance, like so:

    <activity
        android:launchMode="singleInstance"  
        android:name=".MyMainActivity">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
    </activity>

This will avoid attempts to launch different instances of your activity on startup.

***NexenBroadcastReceiver is required to trigger content when user enter/exit geofence notificaion zone.

### Your Application ###

Next thing to do is hook the Nexen SDK into your application.

** The first step is extend our `NexenApplication`. Don't forget to specify your application in the Manifest file. **

The main entry point into the SDK is the 
`NexenProximityManager` singleton. You will need to get the reference and configure it before you can start scanning.

    NexenProximityManager manager = NexenProximityManager.get();
    
    NexenProximitySetup setup = new NexenProximitySetup("my-app-name", "my-app-password");
    manager.configureFor(myApplication, serviceInterface, setup);

Typically you will want to do this in the `onCreate()` method of your `Application` subclass.

Notice how we are passing three arguments into the `configureFor` method:

- myApplication: a reference to the application instance
- serviceInterface: an implementation that will response to callbacks, and provides content to the beacon manager
- setup: configuration parameters
  - WeAreNexen portal credentials (username and password)
  - detect beacons with a [foreground service](https://altbeacon.github.io/android-beacon-library/foreground-service.html). Calling `Application.createForegroundNotification(...)` will create such a notification.
  

You can set list of disabled categories: `NexenApplication.getInstance().getBeaconsFilter().setDisabledCategories(Set<Integer> disabledCategories)`

You can set list tags: `NexenApplication.getInstance().getBeaconsFilter().setTags(Map<String, String> tags)`
Disabled categories and tags will be sent to the server to improve content filtering.


### Implementing the NexenServiceInterface ###

Implementing this interface will tie your application to the operations performed by the beacon manager. we'll cover 
the most important methods here:

Please be aware that when you implement a callback it must not throw any exceptions unless there's something terribly
wrong. An exception will render the state of the SDK unpredictable.
Furthermore, it is not guaranteed that the callback is invoked on the main thread. E.g. when you call
`NexenProximityManager.start()` in the background, then the `onStartMonitoring()` callback is called on that same 
background thread. Make sure to handle these cases and update your UI on the main thread by calling
`Activity.runOnUiThread(Runnable)` within these callbacks.
The reason the callbacks are not invoked on the main thread by the SDK is that this might not always be desirable.
For instance, when you are not updating any UI components it would be pointless, and even yield worse performance, 
to exit the background thread. Even worse when your application needs to perform some heavy lifting or network
activity in response to a callback, doing so on the main thread would yield issues.

#### Scanner callbacks ####

-   `onProximityZoneNotification(BaseNexenZone nexenZone)`

    Once you have started scanning and a beacon has been detected this method will automatically be called.
    The argument beacon reference will also hold information about the content to show.
    Please note that you will not receive invocations of this method for each possible beacon that is somewhere
    in your proximity, only the ones that have been configured in the Nexen server will be 'seen', all others
    will be ignored.
    
    You can check whether parameter `nexenZone` instance of `NexenBeaconZone` or instance of `NexenGeofenceZone`
    `NexenBeaconZone` is zone assigned to particular beacon device.
    This class has info about UUID, Major, Minor and manufacturer of beacon device.
    `NexenGeofenceZone` is zone linked to some geofence zone. 
    This class contains fields about geofence's coordinates and radius.
    
    Note that this method is called with respect to corresponding server-side configuration parameters such as
    dwelling-time (required minimum of seconds the user is in the vicinity of the beacon) and repeat-delay
    (the time to wait after a beacon has been scanned and dismissed by the user before it should be triggered again).
    The beacon manager only triggers beacons for which the content category has been enabled (see the
    `isContentCategoryEnabled(categoryId)` method.

    Calling
 
        showNotificationForProximityZone(BaseNexenZone proximityZone, 
            Class<? extends Activity> targetActivity, 
            int smallIconResId, 
            String contentTitle, 
            int textColor) 

    will show a local notification.
    
    A typical implementation would display a notification when the phone is in standby, bring the app to the foreground
    in case it is in the background, launch the app in case it is not running, or simply push the content on screen.
    Here's an implementation (note that the notification is created regardless of the app status):
    
        if (proximityZone == null || proximityZone.getContent() == null
                || proximityZone.getContent().getMessage() == null
                || proximityZone.getContent().getMessage().trim().length() == 0) {
            return;
        }

        Log.i(TAG, "Triggering content [" + proximityZone.getContent() + "]. New dummy text");

        // set a local notification in case the app is running in the background
        showNotificationForProximityZone(proximityZone,
                TabbedNavigationActivity.class,
                R.drawable.pharos_notification_icon,
                getResources().getString(R.string.notificationTitle),
                ContextCompat.getColor(BeaconApplication.this, R.color.orange));

        if (DataModel.get().isAppIsInForeground()) {
            Log.d(TAG,  "Execute notification task");
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,  "Execute notification task. Started.");
                    // trigger viewing the content
                    final Intent intent = new Intent(BeaconApplication.this, TabbedNavigationActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(GEOFENCE_ZONE, proximityZone instanceof NexenGeofenceZone);
                    intent.putExtra(BEACON_ID_INTENT_DATA_KEY, proximityZone.getId());
                    intent.putExtra(CONTENT_ID_INTENT_DATA_KEY, proximityZone.getContent().getId());
                    intent.putExtra(FORCE_CONTENT_VIEW_INTENT_DATA_KEY, false);
                    Log.d(TAG,  "Execute notification task. Start activity.");
                    startActivity(intent);
                }
            });
        }

        Log.i(TAG, "insertContentIfNeeded() [" + proximityZone.getContent() + "]");
        executeInSingleThread(new Runnable() {
            @Override
            public void run() {
                HistoryContentDB.getInstance().insertContentIfNeeded(proximityZone);
            }
        });

To make things easier, and allow clients to startup the app without having to wait for downloads to finish, subsequent
launches will query the content cache and deliver results before any download starts:

    void onProximityZonesLoadedFromCache(Collection<NexenBeaconZone> zones);

When the download process starts it will:

1. download the list of content categories
2. download the list of locations
3. download the list of geofence zones
4. download the list of beacons for each location

Since Android API level 23 (Marshmallow) there is a new permission model that client code needs to implement in order
to have access to some of the device's subsystems. If at any time the app requires access to a certain subsystem it
will invoke the following method, taking a permission argument as defined in `android.Manifest.permission.*`.
This method will not be called on Android devices with a version below Marshmallow as permissions have been granted
at installation-time.

    void onRequirePermissions(String[] permissions, Runnable finished);

At the very least your implementation must call the finished callback handler, as so:

    public void onRequirePermissions(String[] permissions, Runnable finished) {
        finished.run();
    }
    
However, a decent implementation should prompt the user to grant the permissions, and even inform him/her with a 
rationale as to what is the effect of granting or denying said permission.

    public void onRequirePermissions(String[] permissions, Runnable finished) {
        this.finished = finished;
        ActivityCompat.requestPermissions(myActivity, new String[]{permission}, myRequestCode);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == myRequestCode) {
            // do something based on the user's choice,
            // regardless of this choice the callback handler should always be called
            finished.run();
        }
    }

Note that if at any time the SDK notices that a required permission has been revoked the monitoring process will stop,
the `onRequirePermissions` method will be called, and the monitoring process will be restarted automatically.

`void onLocationsLoaded(List<Location> locations, APIError error)` invoked when list of locations received from the server

### Working with the proximity-manager ###

The proximity manager takes care of downloading content for you, it notifies you of any event by means of the 
`NexenServiceInterface` but you might want to query for zones nevertheless, in case you do then you have these
two methods at your disposal:

-   `Map<String, NexenBeaconZone> getAllProximityZonesByKey()()` 

    which returns all known proximity zones, mapped by their key
    note that the returned map is immutable but is 'live', it therefore reflects changes when the proximity manager 
    downloads new information and activates/deactivates beacons
    
-   `NexenBeaconZone getProximityZoneById(Long beaconId)`

    a convenient method to lookup a proximity zone by id, usually you won't be needing this, unless you need 
    a short code to identify a beacon for which to show content, e.g. when displaying a notification
    
-   `CategorySettings getCategorySettings()`
    
    A beacon for which its content category should be ignored will not be triggered. If a beacon's content is already 
    showing then it will not trigger again. The settings instance that is returned by the manager after a call to this
    method allows you to configure categories accordingly
    
-   `NexenGeofenceZone findGeofenceZone(final Long id)`
     which returns a geofence zone, mapped by id
     
-    `Collection<BaseNexenZone> getEnteredZones()` returns list of all NexenGeofenceZone and NexenBeaconZone which user has entered in.


### Working with the BeaconManager ###

The beacon manager takes care of downloading content for you, it notifies you of any event by means of the 
`NexenServiceInterface` but you might want to query for beacons nevertheless, in case you do then you have these
    
### Detecting changes in Bluetooth status ###

The following code is not part of the SDK, however, most of the apps will want to know the current status of the
Bluetooth service, and be informed when it changes. In order to do so it is sufficient to register another receiver
in your `<application>` element:

    <receiver android:name=".BluetoothStateChangeBroadcastReceiver" >
        <intent-filter>
            <action android:name="android.bluetooth.adapter.action.STATE_CHANGED" />
        </intent-filter>
    </receiver>

The implementation is actually surprisingly straightforward:

    public class BluetoothStateChangeBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
    
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final DataModel dataModel = DataModel.get();
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                
                if (state == BluetoothAdapter.STATE_OFF) {
                    dataModel.setBluetoothEnabled(false);
                } else if (state == BluetoothAdapter.STATE_ON) {
                    dataModel.setBluetoothEnabled(true);
                }
            }
        }
    }

Your app should respond to these events by starting/stopping the beacon manager and indicating to the user the Bluetooth
service is offline and therefore beacons will not be scanned.